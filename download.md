---
layout: page
title: Download
permalink: /download/
sitemap: true
---

### Stable release
<a class="btn btn-primary btn-sm" href="/2019/07/16/kaidan-0.4.1/#download">
	<img style="border: none" src="/images/arrow-right.svg" width="25" height="25" />
	Get the latest release
</a>

### Nightly builds
#### Linux

 * [Nightly Flatpak](https://invent.kde.org/kde/kaidan/-/wikis/using/flatpak)

#### Android (experimental)

* [KDE Nightly F-Droid repository](https://community.kde.org/Android/FDroid)
* [APK](https://binary-factory.kde.org/job/Kaidan_android/lastSuccessfulBuild/artifact/kaidan_build_apk-debug.apk)

### Source code
Kaidan's source code can be found at [invent.kde.org](https://invent.kde.org/kde/kaidan).
