---
layout: post
title: "Kaidan 0.2.2 released"
date: 2017-06-19 10:51:00 +02:00
author: lnj
---

This release only exists, because we're planning to rewrite parts of the back-end and to switch to the XMPP client library "gloox" and we didn't want to write things as avatars twice, so we're doing a non-only-bug-fix-release now and then we'll start to work on the rewrite and profiles with avatars. Because rewrite + new features are a bit more work, the release probably will take longer than wanted, as the 0.2 release.

## Changelog

Fixes:
 * RosterPage: Clear TextFields after closing AddContactSheet (#106) (JBB)

Unused back-end features:
 * RosterController: Save lastMessage for each contact (#108) (LNJ)
 * Add database versioning and conversion (#110) (LNJ)
 * Database: Add new roster row `avatarHash` (#112) (LNJ)

Misc:
 * CMake: Add feature summary (#109) (LNJ)

**Download**: [kaidan-v0.2.2.tar.gz](https://invent.kde.org/KDE/kaidan/-/archive/v0.2.2/kaidan-v0.2.2.tar.gz)
