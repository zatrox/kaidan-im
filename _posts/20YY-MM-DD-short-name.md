---
layout: post
title: "Template for Posts"
date: 20YY-MM-DD 20:00:00 +01:00
author: alice, bob
---

# This is a Big Heading

This is the first paragraph belonging to the first heading.

## This is a Small Heading

This is the first paragraph belonging to the second heading.
This belongs to the same paragraph as the line before.

This is the second paragraph belonging to the second heading.
If you want to embed images, you can do that by e.g. the following line:
![]({{ "/images/screenshots/2020-01-10-quick-onboarding.png" | prepend: site.url }})
